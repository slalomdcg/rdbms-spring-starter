package com.slalom.blog.domain.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * Converts between {@link java.time.ZonedDateTime} to {@link java.sql.Timestamp}
 */
@Converter(autoApply = true)
public class ZonedDateTimeConverter implements AttributeConverter<ZonedDateTime, Timestamp> {

    @Override
    public Timestamp convertToDatabaseColumn(final ZonedDateTime entityValue) {
        return entityValue != null ? Timestamp.from(entityValue.toInstant()) : null;
    }

    @Override
    public ZonedDateTime convertToEntityAttribute(final Timestamp databaseValue) {
        if (databaseValue != null) {
            LocalDateTime localDateTime = databaseValue.toLocalDateTime();
            return localDateTime.atZone(ZoneId.systemDefault());
        }
        return null;
    }
}
