package com.slalom.blog.domain;


import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * Blog Model
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Blog {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "blog_id")
    private String id;
    private String title;
    private String content;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private User user;
    @OneToMany
    @JoinColumn(name = "blog_id", referencedColumnName = "blog_id")
    private List<Comment> comments;

    @Column(name = "date_entered")
    @CreatedDate
    private ZonedDateTime dateEntered;
    @Column(name = "date_modified")
    @LastModifiedDate
    private ZonedDateTime dateModified;



    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(final User user) {
        this.user = user;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(final List<Comment> comments) {
        this.comments = comments;
    }

    public ZonedDateTime getDateEntered() {
        return dateEntered;
    }

    public void setDateEntered(final ZonedDateTime dateEntered) {
        this.dateEntered = dateEntered;
    }

    public ZonedDateTime getDateModified() {
        return dateModified;
    }

    public void setDateModified(final ZonedDateTime dateModified) {
        this.dateModified = dateModified;
    }
}
