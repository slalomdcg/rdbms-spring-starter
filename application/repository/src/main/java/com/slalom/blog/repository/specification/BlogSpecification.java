package com.slalom.blog.repository.specification;

import com.slalom.blog.domain.Blog;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Example specifications for blogs
 */
public class BlogSpecification {

    public static Specification<Blog> findByName(final String name) {
        return (final Root<Blog> root, final CriteriaQuery<?> query, final CriteriaBuilder cb) -> {
            Predicate firstName = cb.like(root.get("user").get("firstName"), name);
            Predicate lastName = cb.like(root.get("user").get("lastName"), name);

            return cb.or(firstName, lastName);
        };
    }
}
