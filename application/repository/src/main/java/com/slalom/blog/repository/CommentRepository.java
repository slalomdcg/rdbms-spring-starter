package com.slalom.blog.repository;

import com.slalom.blog.domain.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data Interface for Comments
 */
public interface CommentRepository extends JpaRepository<Comment, String> {
}
