package com.slalom.blog.repository;

import com.slalom.blog.domain.Blog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

/**
 * Spring Data Interface for Blogs
 */
public interface BlogRepository extends JpaRepository<Blog, String>, JpaSpecificationExecutor<Blog> {

    @RestResource(path = "by-name", rel = "by-name")
    @Query("SELECT b FROM Blog b JOIN FETCH b.user u WHERE u.lastName LIKE :name")
    public List<Blog> findAllByUserLastNameContains(@Param("name") final String name);
}
