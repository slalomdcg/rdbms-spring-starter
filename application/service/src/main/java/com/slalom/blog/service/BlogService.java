package com.slalom.blog.service;

import com.slalom.blog.domain.Blog;
import com.slalom.blog.repository.BlogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.slalom.blog.repository.specification.BlogSpecification.findByName;

@Service
public class BlogService {

    public BlogRepository blogRepository;

    @Autowired
    public BlogService(final BlogRepository blogRepository) {
        this.blogRepository = blogRepository;
    }


    public List<Blog> findBlogsByName(final String name) {
        return blogRepository.findAll(findByName(name));
    }
}
