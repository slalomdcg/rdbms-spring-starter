package com.slalom.blog.service;

import org.springframework.data.auditing.DateTimeProvider;

import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by justinv on 4/5/16.
 */
public class AuditingDateTimeProvider implements DateTimeProvider {

    @Override
    public Calendar getNow() {
        return GregorianCalendar.from(ZonedDateTime.now());
    }
}
