package com.slalom.blog;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.slalom.blog.domain.Blog;
import com.slalom.blog.domain.Comment;
import com.slalom.blog.domain.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

@SpringBootApplication
@ComponentScan(basePackages = { "com.slalom.blog" })
public class Application extends RepositoryRestMvcConfiguration {

    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    protected void configureJacksonObjectMapper(final ObjectMapper objectMapper) {
        objectMapper.registerModule(new JavaTimeModule());
    }

    @Override
    protected void configureRepositoryRestConfiguration(final RepositoryRestConfiguration config) {
        config.exposeIdsFor(User.class, Blog.class, Comment.class);
    }
}

