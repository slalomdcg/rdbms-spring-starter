INSERT INTO USER (USER_ID, NAME_FIRST, NAME_LAST, EMAIL) VALUES ('3531fc3c-fb14-11e5-86aa-5e5517507c66', 'Jorge', 'Sampson', 'jsampleton@gmail.com');
INSERT INTO USER (USER_ID, NAME_FIRST, NAME_LAST, EMAIL) VALUES ('45e86d04-fb14-11e5-86aa-5e5517507c66', 'John', 'Smith', 'JSmith@gmail.com');
INSERT INTO USER (USER_ID, NAME_FIRST, NAME_LAST, EMAIL) VALUES ('45e86e12-fb14-11e5-86aa-5e5517507c66', 'Jane', 'Doe', 'janedoe@gmail.com');

INSERT INTO BLOG (BLOG_ID, USER_ID, TITLE, CONTENT, DATE_ENTERED) VALUES ('cde487ab-b4bb-4a0e-9f67-0d13f3e118a5', '3531fc3c-fb14-11e5-86aa-5e5517507c66', '10 Ways to get users to click your blogs today.', 'blog content, blog content.', '2015-12-01 14:20:01');
INSERT INTO BLOG (BLOG_ID, USER_ID, TITLE, CONTENT, DATE_ENTERED) VALUES ('a9a1cc04-3e7d-4ec8-a13b-9d730ffcad2e', '45e86d04-fb14-11e5-86aa-5e5517507c66', 'Check out my first blog!', 'Blog post content here...', '2015-11-01 16:20:01');
INSERT INTO BLOG (BLOG_ID, USER_ID, TITLE, CONTENT, DATE_ENTERED) VALUES ('f55c3a0a-c130-44d9-bfdb-a46e4467386b', '45e86e12-fb14-11e5-86aa-5e5517507c66', 'Here is a simple blog title', 'Here is simple blog content..', '2015-10-01 10:20:01');

INSERT INTO COMMENT (COMMENT_ID, BLOG_ID, USER_ID, CONTENT, DATE_ENTERED) VALUES ('7db93bab-6894-4418-af3f-c6a8c52ff1ce', 'cde487ab-b4bb-4a0e-9f67-0d13f3e118a5', '45e86d04-fb14-11e5-86aa-5e5517507c66', 'This is very insightful, thank you.', '2015-12-01 11:20:01');
INSERT INTO COMMENT (COMMENT_ID, BLOG_ID, USER_ID, CONTENT, DATE_ENTERED) VALUES ('2f35bf8c-fa39-4a25-80c3-b7899c35af53', 'f55c3a0a-c130-44d9-bfdb-a46e4467386b', '45e86d04-fb14-11e5-86aa-5e5517507c66', 'blog comment for you', '2015-11-01 19:20:01');
INSERT INTO COMMENT (COMMENT_ID, BLOG_ID, USER_ID, CONTENT, DATE_ENTERED) VALUES ('3ba0dc3e-c9b3-40f6-b340-1d042ce6c133', 'f55c3a0a-c130-44d9-bfdb-a46e4467386b', '3531fc3c-fb14-11e5-86aa-5e5517507c66', 'very interesting.', '2015-10-01 10:20:01');
INSERT INTO COMMENT (COMMENT_ID, BLOG_ID, USER_ID, CONTENT, DATE_ENTERED) VALUES ('ff67ab1c-4c74-4534-a51a-0e2a359a3fa1', 'cde487ab-b4bb-4a0e-9f67-0d13f3e118a5', '45e86e12-fb14-11e5-86aa-5e5517507c66', 'Wow, this was great advice!', '2015-12-01 20:20:01');

