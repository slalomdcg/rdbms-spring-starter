CREATE TABLE BLOG (BLOG_ID CHAR(36) NOT NULL, USER_ID CHAR(36) NOT NULL, TITLE VARCHAR(72) NOT NULL, CONTENT TEXT NOT NULL, DATE_ENTERED TIMESTAMP NOT NULL, DATE_MODIFIED TIMESTAMP NULL, PRIMARY KEY (BLOG_ID));
CREATE TABLE USER (USER_ID CHAR(36) NOT NULL, NAME_FIRST VARCHAR(36) NOT NULL, NAME_LAST VARCHAR(36) NOT NULL, EMAIL VARCHAR(50) NOT NULL, PRIMARY KEY (USER_ID));
CREATE TABLE COMMENT (COMMENT_ID CHAR (36) NOT NULL, BLOG_ID CHAR(36) NOT NULL, USER_ID CHAR(36) NOT NULL, CONTENT TEXT, DATE_ENTERED TIMESTAMP NOT NULL, DATE_MODIFIED TIMESTAMP NULL, PRIMARY KEY (COMMENT_ID, BLOG_ID, USER_ID));



