RDBMS Primer Exercise
=============

##The Overview  

Create a service to manage “Blog Posts”, where each Blog Post has the following attributes:

* Blog id
* User
    * First name
    * Last name
    * Email
* Title
* Content
* Date entered (automatically generated and immutable)

##Steps to get started

* Start off by cloning this repo and creating a new branch off of master.
* Run `mvn clean package` from within the root of the cloned directory to build the app.
* Run `java -jar application/rest/target/slalom-blog-rest-0.0.1-SNAPSHOT.war` to run the app.
* Once the app has started, navigate to http://localhost:8080/demo/blogs using a REST client of your choice.  This will return a list of blogs we currently have available.

## Day 3 and Day 4 Tasks to Complete

* Implement "Like" functionality within our blog
* Users can indicate they like either a Comment or a Blog
* From a modeling standpoint, we'll need:
    * A new domain model to represent a Like.
    * We'll need to link our new Like model to a specific User as well as a specific Comment OR Blog.
    * It would also be good to track the time the like was created.
* From a searching standpoint, we'll need:
    * The ability to query for a list of all likes associated with a specific Comment OR Blog
    * The ability to provide a count of the number of likes given for a specific Comment OR Blog
    * The ability to find all Blogs a specific user has liked (searching by name)

##Extra Credit!

* Given two user ids as input, write a query to return all Blogs that both users like.

##Technologies

Our blog API uses (at least) the following technologies:

Capability | Tool/Framework | Version
--------------|-----------------------|-----------
Application Language | [Java](http://www.java.com) | 8
Development Framework | [Spring](http://projects.spring.io/spring-framework/) | 4.x
Application Framework | [Spring Boot](http://projects.spring.io/spring-boot/) | 1.x
Data Abstraction | [Spring Data JPA](http://docs.spring.io/spring-data/jpa/docs/1.9.2.RELEASE/reference/html/) | 1.9.x
Rest Abstraction | [Spring Data Rest](http://docs.spring.io/spring-data/rest/docs/2.4.2.RELEASE/reference/html/) | 2.4.x
Dependency Management | [Maven](https://maven.apache.org/) | 3.2.x
Dev App Server | [Embedded Tomcat](http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#boot-features-embedded-container) | 8.x
Data Store | [H2](http://www.h2database.com/html/main.html) | 1.x

##Structure

```
 |pom.xml
 |-application
 |---common
 |-----pom.xml
 |---domain
 |-----pom.xml
 |---repository
 |-----pom.xml
 |---rest
 |-----pom.xml
 |---service
 |-----pom.xml
```